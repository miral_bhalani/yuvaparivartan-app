package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class MultiselectBox extends ActionBarActivity {


    private Context context;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private ListView listm;
    private JSONArray multiselectListSharedArray;
    private Bundle extras;
    private String key_column;
    private String value_column;
    private JSONObject listArray;
    private JSONObject selectedOptions = new JSONObject();
    private AdapterList adapterList;
    private TextView done;


    @Override
    protected void onResume() {
        super.onResume();

    }

    public void commonInitialization() {
        context = MultiselectBox.this;
        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();


        done = (TextView) findViewById(R.id.done);
        listm = (ListView) findViewById(R.id.listm);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multi_select_box);
        commonInitialization();

        extras = getIntent().getExtras();



        try {


            key_column = extras.getString("key_column");
            value_column = extras.getString("value_column");

            multiselectListSharedArray = new JSONArray(extras.getString("multiselect_list"));
            if(extras!=null && extras.containsKey("selected_options")==true) {

                JSONArray selectedOptionInArray = new JSONArray(extras.getString("selected_options"));
                for(int i=0;i<selectedOptionInArray.length();i++)
                {
                    selectedOptions.put(selectedOptionInArray.getString(i),"-");
                }
            }

            listArray = new JSONObject();
            for(int i=0;i<multiselectListSharedArray.length();i++)
            {
                JSONObject multiselectListSharedArraySingle =  multiselectListSharedArray.getJSONObject(i);

                listArray.put(multiselectListSharedArraySingle.getString(key_column),multiselectListSharedArraySingle.getString(value_column));

            }


            adapterList = new AdapterList();
            listm.setAdapter(adapterList);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String selectedoptionsm = "[]";
                JSONArray selectedOptionSNames = selectedOptions.names();
                try {
                    selectedoptionsm = selectedOptionSNames.toString();
                }
                catch (Exception e)
                {
                    selectedoptionsm = "[]";
                    e.printStackTrace();
                }


                Intent in = new Intent();
                in.putExtra("selected_options",selectedoptionsm);
                setResult(1,in);
                finish();

            }
        });



    }


    public class AdapterList extends BaseAdapter
    {
        private final LayoutInflater inflater;
        private TextView name;

        public AdapterList() {
            // TODO Auto-generated constructor stub
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return multiselectListSharedArray.length();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = inflater.inflate(R.layout.multiselect_box_single, null);


            try {

                listArray.getString(multiselectListSharedArray.getJSONObject(position).getString(key_column));
                name = (TextView) convertView.findViewById(R.id.name);
                name.setText(listArray.getString(multiselectListSharedArray.getJSONObject(position).getString(key_column)));

                listArray.getString(multiselectListSharedArray.getJSONObject(position).getString(key_column));



                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        try {

                            if(selectedOptions.has(multiselectListSharedArray.getJSONObject(position).getString(key_column))==true)
                            {
                                selectedOptions.remove(multiselectListSharedArray.getJSONObject(position).getString(key_column));
                            }
                            else {

                                selectedOptions.put(multiselectListSharedArray.getJSONObject(position).getString(key_column), "-");

                            }

                            adapterList.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                if(selectedOptions.has(multiselectListSharedArray.getJSONObject(position).getString(key_column))==true)
                {
                    name.setBackgroundColor(Color.parseColor("#cccc00"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return convertView;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }


}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class CampVisit extends ActionBarActivity {


    private TextView state, district, block;
    public TextView attendance_date,tempDate;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;
    private TextView save;
    private TextView village;


    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private EditText number_of_student_present;
    private EditText phone_number;
    private EditText number_of_target_groups_met;
    private JSONObject agendaInfo;
    private EditText outcome_of_meeting;
    private TextView tentative_end_date;
    private TextView actual_end_date;
    private TextView camp_status;
    private TextView camp_start_date;
    private TextView camp;
    private  JSONObject campDetails = new JSONObject();
    private TextView take_image_for_attendance_sheet;
    private TextView take_image_of_students;
    private EditText no_of_enrollment;
    private EditText number_of_forms_filled;

    public void commonInitialization()
    {
        context = CampVisit.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        village = (TextView) findViewById(R.id.village);
        camp_start_date = (TextView) findViewById(R.id.camp_start_date);
        tentative_end_date = (TextView) findViewById(R.id.tentative_end_date);
        actual_end_date = (TextView) findViewById(R.id.actual_end_date);
        camp_status = (TextView) findViewById(R.id.camp_status);
        camp = (TextView) findViewById(R.id.camp);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);


        no_of_enrollment = (EditText) findViewById(R.id.no_of_enrollment);
        number_of_forms_filled = (EditText) findViewById(R.id.number_of_forms_filled);



        attendance_date = (TextView) findViewById(R.id.attendance_date);

        number_of_student_present = (EditText) findViewById(R.id.number_of_student_present);
        number_of_target_groups_met = (EditText) findViewById(R.id.number_of_target_groups_met);
        outcome_of_meeting = (EditText) findViewById(R.id.outcome_of_meeting);

        save = (TextView) findViewById(R.id.save);

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance);
        commonInitialization();

        agendaInfo = new CommonFunction().getAgendaInfo(context);

        attendance_date.setText(new CommonFunction().getDisplayDate(agendaInfo.optString("agenda_date")));




        //--get camp information from agenda camp id and set it on sharedpreferance
        try {
//            agendaInfo = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"), ""));
            agendaInfo = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"), ""));

            String campId = agendaInfo.getString("camp");
            JSONArray campArray1 =  new MDbHelper().getAll("camp", "", context);
//            camp_facilitator_date.setText(campArray1.getJSONObject(0).getString("agenda_date"));
//            System.out.println("!!!!pankaj_date_from_agenda_array"+campArray1.getJSONObject(0).getString("agenda_date"));
            JSONArray campArray =  new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
            if(campArray.length()!=0)
            {
                JSONObject campObject = campArray.getJSONObject(0);
                sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"),campObject.toString());
                sharededitor.commit();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        try {
            campDetails = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"),""));


            no_of_enrollment.setText(campDetails.getString("no_of_enrollment"));
            number_of_forms_filled.setText(campDetails.getString("number_of_forms_filled"));

            JSONArray states = new MDbHelper().getAll("state"," where id='"+campDetails.optString("state")+"'",context);
            state.setText(states.optJSONObject(0).optString("state"));

            JSONArray districts = new MDbHelper().getAll("district"," where id='"+campDetails.optString("district")+"'",context);
            district.setText(districts.optJSONObject(0).optString("district"));

            JSONArray blocks = new MDbHelper().getAll("block"," where id='"+campDetails.optString("block")+"'",context);
            block.setText(blocks.optJSONObject(0).optString("block"));

            JSONArray vilages = new MDbHelper().getAll("village"," where id='"+campDetails.optString("village")+"'",context);
            village.setText(vilages.optJSONObject(0).optString("name"));


            camp_start_date.setText(new CommonFunction().getDisplayDate(campDetails.optString("camp_start_date")));
            tentative_end_date.setText(new CommonFunction().getDisplayDate(campDetails.optString("tentative_end_date")));
            actual_end_date.setText(new CommonFunction().getDisplayDate(campDetails.optString("actual_end_date")));

            camp_status.setText(campDetails.optString("camp_status"));

            camp.setText(campDetails.optString("camp_code"));


            camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        

        //--SpinnerInitialization and dependancy specification
//        new CommonFunction().setDependancy1(false,context, state,"state","state",null,null,null,null);
//        new CommonFunction().setDependancy1(true, context, district, "district", "district", state, "state", "state", block);
//        new CommonFunction().setDependancy1(true, context, block, "block", "block", district, "district", "district", null);


//        new CommonFunction().setDependancy1(false, context, camp_coordinator, "camp_coordinator_profile", "name", null, null, null, null);


        attendance_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", attendance_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = attendance_date;

            }
        });



        new CommonFunction().imageClick(context, "t_image_of_attendance_sheet", "attendance");
        new CommonFunction().imageClick(context, "t_image_of_students", "attendance");


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if(new CommonFunction().applyFormRules(rulesValidation(),fieldLabels(),true,context).equals("")==true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("camp", campDetails.optString("id"));
                        saveObject.put("attendance_date", attendance_date.getText().toString());
                        saveObject.put("number_of_student_present", number_of_student_present.getText().toString());
                        new CommonFunction().saveInformation(context, "attendance", saveObject);




                        saveObject = new JSONObject();
                        saveObject.put("no_of_enrollment", no_of_enrollment.getText().toString());
                        saveObject.put("number_of_forms_filled", number_of_forms_filled.getText().toString());
                        new CommonFunction().saveInformation(context,"camp",saveObject);



                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("attendance_date",new CommonFunction().rule_required);
            rules.put("number_of_student_present",new CommonFunction().rule_required);
            rules.put("t_image_of_attendance_sheet",new CommonFunction().rule_image_required);
            rules.put("t_image_of_students",new CommonFunction().rule_image_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }

    @Override
    public void onBackPressed() {

        String validationString = "";

        validationString = new CommonFunction().checkRequiredImages(rulesValidation(),"attendance",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }



}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class CampLeaderTraining extends ActionBarActivity {


    private TextView state, district, block;
    public TextView training_date,tempDate,date_of_the_market;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;
    private TextView save;
    private TextView village;


    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private EditText number_of_camp_leaders;
    private EditText phone_number;
    private EditText location_of_training;
    private JSONObject agendaInfo;
    private EditText outcome_of_meeting;

    EditText name_of_the_market,nearest_bank,any_other_land_mark_near_the_market;
    Spinner enrollment_done;
    private TextView take_image;
    private TextView camp_leader_names;

    TextView tempMultiselect;

    public void commonInitialization()
    {
        context = CampLeaderTraining.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        village = (TextView) findViewById(R.id.village);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);
        training_date = (TextView) findViewById(R.id.training_date);
        number_of_camp_leaders = (EditText) findViewById(R.id.number_of_camp_leaders);
        location_of_training = (EditText) findViewById(R.id.location_of_training);
        camp_leader_names = (TextView) findViewById(R.id.camp_leader_names);


        take_image = (TextView) findViewById(R.id.take_image);


        save = (TextView) findViewById(R.id.save);

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camp_leader_training);
        commonInitialization();

        agendaInfo = new CommonFunction().getAgendaInfo(context);

        training_date.setText(new CommonFunction().getDisplayDate(agendaInfo.optString("agenda_date")));

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block", " where id='" + agendaInfo.optString("block") + "'", context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));

        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));




        //--SpinnerInitialization and dependancy specification
//        new CommonFunction().setDependancy1(false,context, state,"state","state",null,null,null,null);
//        new CommonFunction().setDependancy1(true, context, district, "district", "district", state, "state", "state", block);
//        new CommonFunction().setDependancy1(true, context, block, "block", "block", district, "district", "district", null);


//        new CommonFunction().setDependancy1(false, context, camp_coordinator, "camp_coordinator_profile", "name", null, null, null, null);


        training_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", training_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = training_date;

            }
        });


        camp_leader_names.setTag("[]");
        camp_leader_names.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, MultiselectBox.class);
                in.putExtra("key_column", "id");
                in.putExtra("value_column", "name");

                JSONArray campLeaderList = new MDbHelper().getAll("camp_leader_profile", "", context);

                in.putExtra("multiselect_list", campLeaderList.toString());
                in.putExtra("selected_options", camp_leader_names.getTag().toString());
                startActivityForResult(in, 3);

                tempMultiselect = camp_leader_names;
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try {

                    if(new CommonFunction().applyFormRules(rulesValidation(),fieldLabels(),true,context).equals("")==true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("training_date", training_date.getText().toString());
                        saveObject.put("number_of_camp_leaders", number_of_camp_leaders.getText().toString());
                        saveObject.put("location_of_training", location_of_training.getText().toString());
                        saveObject.put("camp_leader_names", camp_leader_names.getTag().toString());

                        new CommonFunction().saveInformation(context,"camp_leader_training",saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });





        new CommonFunction().imageClick(context, "t_image_of_canopy", "camp_leader_training");



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
        if(requestCode==3 && resultCode==1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("selected_options")==true) {
                tempMultiselect.setTag(data.getExtras().getString("selected_options"));

                try {
                    JSONArray selectedOptions = new JSONArray(data.getExtras().getString("selected_options"));
                    tempMultiselect.setText(" - "+selectedOptions.length()+" selected");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("training_date",new CommonFunction().rule_required);
            rules.put("number_of_camp_leaders",new CommonFunction().rule_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }



}

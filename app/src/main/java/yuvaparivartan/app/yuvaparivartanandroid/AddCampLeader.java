package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class AddCampLeader extends ActionBarActivity {

    public TextView camp_start_date, tentative_end_date, actual_end_date, tempDate;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private JSONObject agendaInfo;
    private EditText name;
    private EditText address;
    private TextView save;
    private Spinner course;
    private TextView t_image_of_camp_leader,title_inpage;
    private EditText age;

    private TextView state;
    private TextView district;
    private TextView block, area_manager, camp_coordinator,date_of_birth;
    EditText email_id, mobile_number, prior_work_experiance;
    EditText pan_number;
    Spinner gender,education_qualification;
    private TextView take_image_image_of_interview_form,take_image_image_of_address_proof,take_image_image_of_id_proof,take_image_image_of_last_education_qualification;
    private EditText base_location;
    private EditText alternate_contact_number;
    private Spinner induction_provided;
    private TextView date_of_induction;
    private LinearLayout induction_date_lin;


    public void commonInitialization()
    {
        context = AddCampLeader.this;
        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        name = (EditText) findViewById(R.id.name);

        area_manager = (TextView) findViewById(R.id.area_manager);
        camp_coordinator = (TextView) findViewById(R.id.camp_coordinator);

        induction_provided= (Spinner) findViewById(R.id.induction_provided);

        base_location = (EditText) findViewById(R.id.base_location);
        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        camp_coordinator = (TextView) findViewById(R.id.camp_coordinator);

        email_id = (EditText) findViewById(R.id.email_id);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        alternate_contact_number = (EditText) findViewById(R.id.alternate_contact_number);



        date_of_induction = (TextView) findViewById(R.id.date_of_induction);



        gender = (Spinner) findViewById(R.id.gender);
        date_of_birth = (TextView) findViewById(R.id.date_of_birth);
        pan_number = (EditText) findViewById(R.id.pan_number);
        education_qualification = (Spinner) findViewById(R.id.education_qualification);
        prior_work_experiance = (EditText) findViewById(R.id.prior_work_experiance);



        induction_date_lin = (LinearLayout) findViewById(R.id.induction_date_lin);




        address = (EditText) findViewById(R.id.address);
        age = (EditText) findViewById(R.id.age);

        save = (TextView) findViewById(R.id.save);
        t_image_of_camp_leader = (TextView) findViewById(R.id.t_image_of_camp_leader);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_camp_leader);
        commonInitialization();

//        new CommonFunction().setDependancy1(false, context, area_manager, "area_manager_profile", "name", null, null, null, null);
//        new CommonFunction().setDependancy1(true, context, camp_coordinator, "camp_coordinator_profile", "name", area_manager, "area_manager", "area_manager_profile", null);
//        new CommonFunction().setInformationInFormIfAvailable("camp_leader_profile", context);




        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.camp_introduction_provided, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        induction_provided.setAdapter(adapter);


        adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);




        adapter = ArrayAdapter.createFromResource(this, R.array.education_qualification, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        education_qualification.setAdapter(adapter);






        agendaInfo = new CommonFunction().getAgendaInfo(context);

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block", " where id='" + agendaInfo.optString("block") + "'", context);
        block.setText(blocks.optJSONObject(0).optString("block"));




        area_manager.setText(new CommonFunction().getAreaManagerId(context));
        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));




        date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_birth.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_birth;

            }
        });

        date_of_induction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_induction.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_induction;

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("name", name.getText().toString());
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("area_manager", area_manager.getText().toString());
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("base_location", base_location.getText().toString());


                        saveObject.put("induction_provided", induction_provided.getSelectedItem().toString().trim());
                        saveObject.put("date_of_induction", date_of_induction.getText().toString());

                        saveObject.put("email_id", email_id.getText().toString());
                        saveObject.put("mobile_number", mobile_number.getText().toString());
                        saveObject.put("alternate_contact_number", alternate_contact_number.getText().toString());

                        saveObject.put("gender", gender.getSelectedItem().toString());
                        saveObject.put("date_of_birth", date_of_birth.getText().toString());
                        saveObject.put("pan_number", pan_number.getText().toString());
                        saveObject.put("education_qualification", education_qualification.getSelectedItem().toString());
                        saveObject.put("prior_work_experiance", prior_work_experiance.getText().toString());

                        saveObject.put("address", address.getText().toString());
                        saveObject.put("age", age.getText().toString());

                        // new CommonFunction().saveInformation(context, "camp_facilitator", saveObject);

                        new CommonFunction().saveInformation(context, "camp_leader_profile", saveObject);

                        new CommonFunction().showAlertDialog(new CommonFunction().data_saved_successfully, "", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



//        image_of_camp_leader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                try {
//                    if (new CommonFunction().isDetailSavedOnce(context, "camp_leader_profile") == true) {
//
//                        sharededitor.putString(new CommonFunction().take_image_table_name, "camp_leader_profile");
//                        sharededitor.putString(new CommonFunction().take_image_column_name, "photo");
//                        sharededitor.putString(new CommonFunction().take_image_row_id, new CommonFunction().getCurrentTableInformationIfAvailable(context, "camp_leader_profile").getString("id"));
//                        sharededitor.commit();
//
//                        Intent in = new Intent(AddCampLeader.this, TakeOrPickImage.class);
//                        startActivity(in);
//                    } else {
//                        new CommonFunction().showAlertDialog("Please save information first", "", context);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        new CommonFunction().imageClick(context, "t_image_of_camp_leader", "camp_leader_profile");
        new CommonFunction().imageClick(context, "t_image_of_interview_form", "camp_leader_profile");
        new CommonFunction().imageClick(context, "t_image_of_address_proof", "camp_leader_profile");
        new CommonFunction().imageClick(context, "t_image_of_id_proof", "camp_leader_profile");
        new CommonFunction().imageClick(context, "t_image_of_last_educational_qualification", "camp_leader_profile");




        changeVisibilityOfSomeLin(induction_provided, induction_date_lin, "Yes", "No");
        induction_provided.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeVisibilityOfSomeLin(induction_provided, induction_date_lin, "Yes", "No");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
        }
        else if(controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
        }
    }



    public void imageClick(final String columnName, final String tableName)
    {
        String packageName = getPackageName();
        int resId = context.getResources().getIdentifier(columnName, "id", context.getPackageName());

        TextView columnNameV = (TextView) findViewById(resId);
        columnNameV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (new CommonFunction().isDetailSavedOnce(context, tableName) == true) {

                        sharededitor.putString(new CommonFunction().take_image_table_name, tableName);
                        sharededitor.putString(new CommonFunction().take_image_column_name, columnName);
                        sharededitor.putString(new CommonFunction().take_image_row_id, new CommonFunction().getCurrentTableInformationIfAvailable(context, tableName).getString("id"));
                        sharededitor.commit();

                        Intent in = new Intent(AddCampLeader.this, TakeOrPickImage.class);
                        startActivity(in);
                    } else {
                        new CommonFunction().showAlertDialog("Please save information first", "", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }

    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("name", new CommonFunction().rule_required);
            rules.put("mobile_number",new CommonFunction().rule_min_length+"=10");
            rules.put("alternate_contact_number",new CommonFunction().rule_min_length+"=10");

            rules.put("email_id",new CommonFunction().rule_email);
            rules.put("base_location",new CommonFunction().rule_required);
            rules.put("induction_provided",new CommonFunction().rule_required);


            rules.put("t_image_of_camp_leader",new CommonFunction().rule_image_required);
            rules.put("t_image_of_interview_form",new CommonFunction().rule_image_required);
            rules.put("t_image_of_address_proof",new CommonFunction().rule_image_required);
            rules.put("t_image_of_id_proof",new CommonFunction().rule_image_required);
            rules.put("t_image_of_last_educational_qualification",new CommonFunction().rule_image_required);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {

            labels.put("name","Name");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return labels;
    }



    @Override
    public void onBackPressed() {

        String validationString = "";


        CommonFunction commonFunction = new CommonFunction();
        commonFunction.subActivity = true;
        validationString = commonFunction.checkRequiredImages(rulesValidation(),"meet_key_person",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }
}

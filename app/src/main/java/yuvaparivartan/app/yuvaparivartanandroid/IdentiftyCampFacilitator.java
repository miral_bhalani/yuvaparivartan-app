package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class IdentiftyCampFacilitator extends ActionBarActivity {


    public TextView tempDate;
    private TextView state, district, block;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;

    private TextView save;
    private TextView village;
    private EditText camp_code;
    private TextView add_new_facilitator;

    public JSONObject agendaSingle = new JSONObject();

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private JSONObject agendaInfo;
    private LinearLayout camp_facilitator_dropdownlin;
    private Spinner camp_facilitator_induction_provided;

    private Spinner camp_facilitator;
    private TextView camp_facilitator_date;

    private TextView camp_facilitator_induction_date;
    private RelativeLayout add_newfacilitator_buttonlin;
    private Spinner existing_camp_facilitator;
    private LinearLayout induction_date_lin;



    public void commonInitialization()
    {
        context = IdentiftyCampFacilitator.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);

        camp_facilitator = (Spinner) findViewById(R.id.camp_facilitator);
        village = (TextView) findViewById(R.id.village);
        camp_code = (EditText) findViewById(R.id.camp_code);
        camp_facilitator_date = (TextView) findViewById(R.id.camp_facilitator_date);

        existing_camp_facilitator = (Spinner) findViewById(R.id.existing_camp_facilitator);
        camp_facilitator_dropdownlin = (LinearLayout) findViewById(R.id.camp_facilitator_dropdownlin);

        add_newfacilitator_buttonlin = (RelativeLayout) findViewById(R.id.add_newfacilitator_buttonlin);
        camp_facilitator_induction_provided = (Spinner) findViewById(R.id.camp_facilitator_induction_provided);

        camp_facilitator_induction_date = (TextView) findViewById(R.id.camp_facilitator_induction_date);
        induction_date_lin = (LinearLayout) findViewById(R.id.induction_date_lin);

        add_new_facilitator = (TextView) findViewById(R.id.add_new_facilitator);
        save = (TextView) findViewById(R.id.save);

    }



    TextView tempMultiselect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.identify_camp_facilitator);
        commonInitialization();

        /* Display Date in correct format in agenda detail page - pass Shared Preferences and TextView and db field name for your concern
         * For agenda = agenda_date
         * Camp Start Date = camp_start_date
         * Tentative End Date = tentative_end_date
         * Actual End Date = actual_end_date;
         */


        agendaInfo = new CommonFunction().getAgendaInfo(context);

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block"," where id='"+agendaInfo.optString("block")+"'",context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));


        String campId = "";
        //--get camp information from agenda camp id and set it on sharedpreferance
        try {
            campId = agendaInfo.getString("camp");

            if(campId.equals("")==false) {
                JSONArray campArray1 = new MDbHelper().getAll("camp", "", context);
                //            camp_facilitator_date.setText(campArray1.getJSONObject(0).getString("agenda_date"));
                //            System.out.println("!!!!pankaj_date_from_agenda_array"+campArray1.getJSONObject(0).getString("agenda_date"));
                JSONArray campArray = new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
                if (campArray.length() != 0) {
                    JSONObject campObject = campArray.getJSONObject(0);
                    sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"), campObject.toString());
                    sharededitor.commit();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.existing_camp_leader, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        existing_camp_facilitator.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.camp_introduction_provided, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        camp_facilitator_induction_provided.setAdapter(adapter);
        camp_facilitator_induction_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", camp_facilitator_induction_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = camp_facilitator_induction_date;

            }
        });


        new CommonFunction().DisplayDateAgendaDetailPage(sharedpreferences, camp_facilitator_date, "agenda_date");


        new CommonFunction().setDependancy1(false, context, camp_facilitator, "camp_facilitator", "name", null, null, null, null);



        add_new_facilitator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CommonFunction().whileAddFormRemoveSaveInforFromSharedPreferencce(context, "camp_facilitator");
                Intent in = new Intent(IdentiftyCampFacilitator.this, AddCampFacilitator.class);
                startActivityForResult(in, 2);
            }
        });




        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("camp_code", camp_code.getText().toString());
                        saveObject.put("existing_camp_facilitator", existing_camp_facilitator.getSelectedItem().toString().trim());
                        saveObject.put("camp_facilitator_induction_provided", camp_facilitator_induction_provided.getSelectedItem().toString().trim());
                        saveObject.put("camp_facilitator", new CommonFunction().getSelectedItemIdFromSpinner(camp_facilitator, "camp_facilitator", context));
                        saveObject.put("camp_facilitator_date", camp_facilitator_date.getText().toString());
                        saveObject.put("camp_facilitator_induction_date", camp_facilitator_induction_date.getText().toString());


                        new CommonFunction().saveInformation(context, "camp", saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialogForSave("Data Saved successfully", "", context);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



        changeVisibilityOfSomeLin(existing_camp_facilitator,  camp_facilitator_dropdownlin, "Yes", "No");
        changeVisibilityOfSomeLin(existing_camp_facilitator, add_newfacilitator_buttonlin, "No", "Yes");
        existing_camp_facilitator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                changeVisibilityOfSomeLin(existing_camp_facilitator,  camp_facilitator_dropdownlin, "Yes", "No");
                changeVisibilityOfSomeLin(existing_camp_facilitator, add_newfacilitator_buttonlin, "No", "Yes");

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        changeVisibilityOfSomeLin(camp_facilitator_induction_provided, induction_date_lin, "Yes", "No");
        camp_facilitator_induction_provided.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeVisibilityOfSomeLin(camp_facilitator_induction_provided, induction_date_lin, "Yes", "No");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));

        if(campId.equals("")==false) {
            new CommonFunction().setInformationInFormIfAvailable("camp", context);
        }
        else
        {
            sharededitor.remove(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"));
            sharededitor.commit();
        }


    }


    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
        }
        else if(controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
        if(requestCode == 2)
        {
            new CommonFunction().setDependancy1(false, context, camp_facilitator, "camp_facilitator", "name", null, null, null, null);

            try {

                JSONObject createdCampFacilitator = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp_facilitator"), ""));
                new CommonFunction().setSelectedItemIdToSpinner(camp_facilitator,"camp_facilitator",createdCampFacilitator.getString("id"),context);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("camp_code",new CommonFunction().rule_required);

            rules.put("camp_facilitator_date",new CommonFunction().rule_required);
            rules.put("existing_camp_facilitator",new CommonFunction().rule_required);
            rules.put("camp_facilitator_induction_provided",new CommonFunction().rule_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }



}

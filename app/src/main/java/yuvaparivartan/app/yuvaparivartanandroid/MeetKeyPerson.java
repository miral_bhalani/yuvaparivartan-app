package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class MeetKeyPerson extends ActionBarActivity {


    private TextView state, district, block;
    public TextView meeting_date,tempDate,date_of_the_market;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;
    private Spinner role;
    private TextView save;
    private TextView village;


    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private EditText name_of_person;
    private EditText phone_number;
    private JSONObject agendaInfo;
    private TextView take_image,title_inpage;

    EditText name_of_the_market,nearest_bank,any_other_land_mark_near_the_market;
    Spinner enrollment_done;
    private TextView take_image_mobilization;

    public void commonInitialization()
    {
        context = MeetKeyPerson.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        village = (TextView) findViewById(R.id.village);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);
        meeting_date = (TextView) findViewById(R.id.meeting_date);
        date_of_the_market = (TextView) findViewById(R.id.date_of_the_market);
        name_of_person = (EditText) findViewById(R.id.name_of_person);
        phone_number = (EditText) findViewById(R.id.phone_number);
        role = (Spinner) findViewById(R.id.role);
        take_image = (TextView) findViewById(R.id.take_image);


        name_of_the_market = (EditText) findViewById(R.id.name_of_the_market);
        date_of_the_market = (TextView) findViewById(R.id.date_of_the_market);
        nearest_bank = (EditText) findViewById(R.id.nearest_bank);
        any_other_land_mark_near_the_market = (EditText) findViewById(R.id.any_other_land_mark_near_the_market);

        enrollment_done = (Spinner) findViewById(R.id.enrollment_done);

        save = (TextView) findViewById(R.id.save);

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meet_key_person);
        commonInitialization();



        agendaInfo = new CommonFunction().getAgendaInfo(context);

        meeting_date.setText(new CommonFunction().getDisplayDate(agendaInfo.optString("agenda_date")));

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block"," where id='"+agendaInfo.optString("block")+"'",context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));

        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));


        //--SpinnerInitialization and dependancy specification
//        new CommonFunction().setDependancy1(false,context, state,"state","state",null,null,null,null);
//        new CommonFunction().setDependancy1(true, context, district, "district", "district", state, "state", "state", block);
//        new CommonFunction().setDependancy1(true, context, block, "block", "block", district, "district", "district", null);
//
//
//        new CommonFunction().setDependancy1(false, context, camp_coordinator, "camp_coordinator_profile", "name", null, null, null, null);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.meet_key_person_role, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        role.setAdapter(adapter);



        adapter = ArrayAdapter.createFromResource(this, R.array.enrollment_done, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        enrollment_done.setAdapter(adapter);


        date_of_the_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_the_market.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_the_market;

            }
        });





        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("meeting_date", meeting_date.getText().toString());
                        saveObject.put("name_of_person", name_of_person.getText().toString());
                        saveObject.put("role", role.getSelectedItem().toString());
                        saveObject.put("phone_number", phone_number.getText().toString());

                        //---
                        saveObject.put("name_of_the_market", name_of_the_market.getText().toString());
                        saveObject.put("date_of_the_market", date_of_the_market.getText().toString());
                        saveObject.put("nearest_bank", nearest_bank.getText().toString());
                        saveObject.put("any_other_land_mark_near_the_market", any_other_land_mark_near_the_market.getText().toString());
                        saveObject.put("enrollment_done", enrollment_done.getSelectedItem().toString());

                        new CommonFunction().saveInformation(context, "meet_key_person", saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



        new CommonFunction().imageClick(context, "t_image_of_key_person", "meet_key_person");
        new CommonFunction().imageClick(context, "t_image_of_canopy", "meet_key_person");


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("meeting_date",new CommonFunction().rule_required);
            rules.put("name_of_person",new CommonFunction().rule_required);
            rules.put("phone_number",new CommonFunction().rule_required);
            rules.put("phone_number",new CommonFunction().rule_min_length+"=10");
            rules.put("t_image_of_key_person",new CommonFunction().rule_image_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }




    @Override
    public void onBackPressed() {

        String validationString = "";

        validationString = new CommonFunction().checkRequiredImages(rulesValidation(),"meet_key_person",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }




}

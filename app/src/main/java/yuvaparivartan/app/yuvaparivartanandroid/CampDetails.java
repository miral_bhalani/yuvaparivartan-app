package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class CampDetails extends ActionBarActivity {


    private TextView state, district, block;
    public TextView camp_start_date, tentative_end_date, actual_end_date, tempDate;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;
    private Spinner camp_status;
    private TextView save;
    private TextView village;
    private EditText camp_code;
    private EditText no_of_enrollment;
    public JSONObject agendaSingle = new JSONObject();

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private JSONObject agendaInfo;
    private TextView course;
    private EditText amount_of_fees_collected;
    private Spinner existing_camp_leader;
    private LinearLayout camp_leader_dropdownlin;
    private RelativeLayout add_newleader_button;
    private Spinner camp_leader;

    public void commonInitialization()
    {
        context = CampDetails.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);

        village = (TextView) findViewById(R.id.village);
        camp_code = (EditText) findViewById(R.id.camp_code);


        camp_leader = (Spinner) findViewById(R.id.camp_leader);
        amount_of_fees_collected = (EditText) findViewById(R.id.camp_code);

        camp_leader_dropdownlin = (LinearLayout) findViewById(R.id.camp_leader_dropdownlin);


        camp_status = (Spinner) findViewById(R.id.camp_status);

        camp_start_date = (TextView) findViewById(R.id.camp_start_date);
        tentative_end_date = (TextView) findViewById(R.id.tentative_end_date);
        actual_end_date = (TextView) findViewById(R.id.actual_end_date);
        no_of_enrollment = (EditText) findViewById(R.id.no_of_enrollment);

        add_newleader_button= (RelativeLayout) findViewById(R.id.add_newleadder_button);

        existing_camp_leader= (Spinner) findViewById(R.id.existing_camp_leader);


        course = (TextView) findViewById(R.id.course);
        
        save = (TextView) findViewById(R.id.save);

    }



    TextView tempMultiselect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.campdetails);
        commonInitialization();

        /* Display Date in correct format in agenda detail page - pass Shared Preferences and TextView and db field name for your concern
         * For agenda = agenda_date
         * Camp Start Date = camp_start_date
         * Tentative End Date = tentative_end_date
         * Actual End Date = actual_end_date;
         */


        agendaInfo = new CommonFunction().getAgendaInfo(context);

        camp_start_date.setText(new CommonFunction().getDisplayDate(agendaInfo.optString("agenda_date")));

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block"," where id='"+agendaInfo.optString("block")+"'",context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));




        //--get camp information from agenda camp id and set it on sharedpreferance
        try {
            agendaInfo = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"), ""));

            String campId = agendaInfo.getString("camp");


            JSONArray campArray1 =  new MDbHelper().getAll("camp", "", context);

            JSONArray campArray =  new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
            if(campArray.length()!=0)
            {
                JSONObject campObject = campArray.getJSONObject(0);
                sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"),campObject.toString());
                sharededitor.commit();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        new CommonFunction().setInformationInFormIfAvailable("camp", context);


//        try {
//            agendaSingle = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"),""));
//            camp_start_date.setText(new CommonFunction().changeDateFormat(agendaSingle.getString("agenda_date"),"yyyy-mm-dd","dd-mm-yyyy"));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

 //       System.out.println("!!!!pankaj_agenda_single"+agendaSingle);


//        sharededitor.remove(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"));
//        sharededitor.commit();

        new CommonFunction().setDependancy1(false, context, camp_leader, "camp_leader_profile", "name", null, null, null, null);

//        new CommonFunction().setDependancy1(false, context, course, "courses", "name", null, null, null, null);
        //--SpinnerInitialization and dependancy specification
//        new CommonFunction().setDependancy1(false,context, state,"state","state",null,null,null,null);
//        new CommonFunction().setDependancy1(true, context, district, "district", "district", state, "state", "state", block);
//        new CommonFunction().setDependancy1(true,context, block,"block","block", district,"district","district",null);


//        new CommonFunction().setDependancy1(false, context, camp_leader, "camp_leader_profile", "name", null, null, null, null);


        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.camp_status, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        camp_status.setAdapter(adapter);
        camp_status.setEnabled(false);

        camp_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", camp_start_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = camp_start_date;

            }
        });


        tentative_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", tentative_end_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = tentative_end_date;
            }
        });


        actual_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", tentative_end_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = actual_end_date;
            }
        });



        course.setTag("[]");
        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, MultiselectBox.class);
                in.putExtra("key_column", "id");
                in.putExtra("value_column", "name");

                JSONArray multiselectList = new MDbHelper().getAll("courses", "", context);

                in.putExtra("multiselect_list", multiselectList.toString());
                in.putExtra("selected_options", course.getTag().toString());
                startActivityForResult(in, 3);

                tempMultiselect = course;
            }
        });


        adapter = ArrayAdapter.createFromResource(this, R.array.existing_camp_leader, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        existing_camp_leader.setAdapter(adapter);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("camp_leader", new CommonFunction().getSelectedItemIdFromSpinner(camp_leader, "camp_leader_profile", context));
                        saveObject.put("camp_code", camp_code.getText().toString());
                        saveObject.put("camp_start_date", camp_start_date.getText().toString());
                        saveObject.put("tentative_end_date", tentative_end_date.getText().toString());
                        saveObject.put("actual_end_date", actual_end_date.getText().toString());
                        saveObject.put("existing_camp_leader", existing_camp_leader.getSelectedItem().toString().trim());
                        saveObject.put("camp_status", camp_status.getSelectedItem().toString());
                        saveObject.put("course", course.getTag().toString());
                        saveObject.put("no_of_enrollment", no_of_enrollment.getText().toString());
                        saveObject.put("amount_of_fees_collected", amount_of_fees_collected.getText().toString());

                        new CommonFunction().saveInformation(context, "camp", saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialogForSave("Data Saved successfully", "", context);


//                        Intent in = new Intent(context,AllactivitiesPageActivity.class);
//                        in.putExtra("form_name","Todays Activity");
//                        in.putExtra("where_clause"," where agenda_date='"+new CommonFunction().getCurrentDateInFormat()+"'");
//                        startActivity(in);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });





        changeVisibilityOfSomeLin(existing_camp_leader, camp_leader_dropdownlin, "Yes", "No");
        changeVisibilityOfSomeLin(existing_camp_leader, add_newleader_button, "No", "Yes");
        existing_camp_leader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeVisibilityOfSomeLin(existing_camp_leader, camp_leader_dropdownlin, "Yes", "No");
                changeVisibilityOfSomeLin(existing_camp_leader, add_newleader_button, "No", "Yes");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        add_newleader_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CommonFunction().whileAddFormRemoveSaveInforFromSharedPreferencce(context, "camp_leader_profile");

                Intent in = new Intent(context, AddCampLeader.class);
                startActivityForResult(in, 2);
            }
        });

    }


    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
        }
        else if(controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
        if(requestCode == 2)
        {
            new CommonFunction().setDependancy1(false, context, camp_leader, "camp_leader_profile", "name", null, null, null, null);

            try {

                JSONObject createdCampLeader = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp_leader_profile"), ""));
                new CommonFunction().setSelectedItemIdToSpinner(camp_leader,"camp_leader_profile",createdCampLeader.getString("id"),context);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(requestCode==3 && resultCode==1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("selected_options")==true) {
                tempMultiselect.setTag(data.getExtras().getString("selected_options"));

                try {
                    JSONArray selectedOptions = new JSONArray(data.getExtras().getString("selected_options"));
                    tempMultiselect.setText(""+selectedOptions.length()+" courses selected");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }


    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("camp_code",new CommonFunction().rule_required);
            rules.put("camp_start_date",new CommonFunction().rule_required);
            rules.put("tentative_end_date",new CommonFunction().rule_required);
            rules.put("actual_end_date",new CommonFunction().rule_required);
            rules.put("no_of_enrollment",new CommonFunction().rule_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }



}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;

public class Camp_Leader_Appointed extends ActionBarActivity {

    private Spinner camp_leader,camp_leader_induction_provided;
    public TextView camp_leader_date, tempDate,induction_date;
    private TextView state, district, block;

    private RelativeLayout add_newleader_button;
    private TextView save;

    SpAdapter adapter;
    List<String> listServ = new ArrayList<>();
    private Context context;
    private String last_selecteddate;
    private TextView camp_status;
    private TextView village;
    private TextView camp_code;
    private JSONObject agendaInfo;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private Spinner existing_camp_leader;
    private LinearLayout induction_provided_lin;
    private LinearLayout induction_date_lin;
    private LinearLayout camp_leader_dropdownlin;


    public void commonInitialization() {
        context = Camp_Leader_Appointed.this;
        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        camp_status = (TextView) findViewById(R.id.camp_status);
        village = (TextView) findViewById(R.id.village);
        camp_code = (TextView) findViewById(R.id.camp_code);
        camp_leader = (Spinner) findViewById(R.id.camp_leader);
        existing_camp_leader= (Spinner) findViewById(R.id.existing_camp_leader);
        camp_leader_induction_provided= (Spinner) findViewById(R.id.camp_leader_induction_provided);
        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);

        induction_provided_lin = (LinearLayout) findViewById(R.id.induction_provided_lin);
        induction_date_lin = (LinearLayout) findViewById(R.id.induction_date_lin);
        camp_leader_dropdownlin = (LinearLayout) findViewById(R.id.camp_leader_dropdownlin);


        camp_leader_date = (TextView) findViewById(R.id.camp_leader_date);

        induction_date = (TextView) findViewById(R.id.induction_date);

        add_newleader_button= (RelativeLayout) findViewById(R.id.add_newleadder_button);
        save = (TextView) findViewById(R.id.save);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.campleaderappointed);
        commonInitialization();

        try {
            agendaInfo = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"), ""));
            String campId = agendaInfo.getString("camp");
            JSONArray campArray1 =  new MDbHelper().getAll("camp", "", context);
            JSONArray campArray =  new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
            if(campArray.length()!=0)
            {
                JSONObject campObject = campArray.getJSONObject(0);
                sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"),campObject.toString());
                sharededitor.commit();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        /* Display Date in correct format in agenda detail page - pass Shared Preferences and TextView  */
        new CommonFunction().DisplayDateAgendaDetailPage(sharedpreferences, camp_leader_date,"agenda_date");

        new CommonFunction().setDependancy1(false, context, camp_leader, "camp_leader_profile", "name", null, null, null, null);
        new CommonFunction().setInformationInFormIfAvailable("camp", context);



        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.existing_camp_leader, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        existing_camp_leader.setAdapter(adapter);



//        adapter = ArrayAdapter.createFromResource(this, R.array.camp_introduction_provided, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        camp_leader_induction_provided.setAdapter(adapter);



//        existing_camp_leader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                changeVisibilityOfInductionProvidedLin();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//
//        camp_leader_induction_provided.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                changeVisibilityOfInductionDateLin();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


//        camp_leader_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent in = new Intent(context, DatePickerM.class);
//                in.putExtra("date", camp_leader_date.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
//                startActivityForResult(in, 1);
//                tempDate = camp_leader_date;
//
//            }
//        });

//        induction_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent in = new Intent(context, DatePickerM.class);
//                in.putExtra("date", induction_date.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
//                startActivityForResult(in, 1);
//                tempDate = induction_date;
//
//            }
//        });

        add_newleader_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CommonFunction().whileAddFormRemoveSaveInforFromSharedPreferencce(context, "camp_leader_profile");

                Intent in = new Intent(Camp_Leader_Appointed.this, AddCampLeader.class);
                startActivityForResult(in, 2);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("camp_leader", new CommonFunction().getSelectedItemIdFromSpinner(camp_leader, "camp_leader_profile", context));
                        saveObject.put("camp_leader_date", camp_leader_date.getText().toString());
//                        saveObject.put("camp_leader_induction_provided", camp_leader_induction_provided.getSelectedItem().toString().trim());
                        saveObject.put("existing_camp_leader", existing_camp_leader.getSelectedItem().toString().trim());
//                        saveObject.put("induction_date", induction_date.getText().toString());

                        new CommonFunction().saveInformation(context, "camp", saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });





        changeVisibilityOfSomeLin(existing_camp_leader, camp_leader_dropdownlin, "Yes", "No");
        changeVisibilityOfSomeLin(existing_camp_leader, add_newleader_button, "No", "Yes");
        existing_camp_leader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeVisibilityOfSomeLin(existing_camp_leader, camp_leader_dropdownlin, "Yes", "No");
                changeVisibilityOfSomeLin(existing_camp_leader, add_newleader_button, "No", "Yes");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



//        changeVisibilityOfSomeLin(camp_leader_induction_provided, induction_date_lin, "Yes", "No");
//        camp_leader_induction_provided.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                changeVisibilityOfSomeLin(camp_leader_induction_provided, induction_date_lin, "Yes", "No");
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });



//        changeVisibilityOfInductionProvidedLin();
//        changeVisibilityOfInductionDateLin();

    }

    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
        }
        else if(controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
        }
    }

    public void changeVisibilityOfInductionProvidedLin()
    {
        if(existing_camp_leader.getSelectedItem().toString().equals("Yes"))
        {
            induction_provided_lin.setVisibility(View.VISIBLE);
        }
        else
        {
            induction_provided_lin.setVisibility(View.GONE);
        }
    }

    public void changeVisibilityOfInductionDateLin()
    {
        if(camp_leader_induction_provided.getSelectedItem().toString().equals("Yes"))
        {
            induction_date_lin.setVisibility(View.VISIBLE);
        }
        else
        {
            induction_date_lin.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));



//        new CommonFunction().setInformationInFormIfAvailable("camp", context);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
        if(requestCode == 2)
        {
            new CommonFunction().setDependancy1(false, context, camp_leader, "camp_leader_profile", "name", null, null, null, null);

            try {

                JSONObject createdCampLeader = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp_leader_profile"), ""));
                new CommonFunction().setSelectedItemIdToSpinner(camp_leader,"camp_leader_profile",createdCampLeader.getString("id"),context);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("camp_leader_date",new CommonFunction().rule_required);
            rules.put("existing_camp_leader",new CommonFunction().rule_required);
//            rules.put("camp_leader_induction_provided",new CommonFunction().rule_required);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {

            labels.put("camp_leader_date","Date");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return labels;
    }

}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import yuvaparivartan.app.yuvaparivartanandroid.SpAdapter;
import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class AddCampFacilitator extends ActionBarActivity {

    public TextView camp_start_date, tentative_end_date, actual_end_date, tempDate;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private JSONObject agendaInfo;
    private EditText name;
    private EditText address;
    private TextView save;
    private TextView course;
    private TextView take_image,title_inpage;
    private TextView state;
    private TextView district;
    private TextView block, area_manager, camp_coordinator,date_of_birth;
    EditText email_id, mobile_number, prior_work_experiance;
    EditText pan_number;
    Spinner gender, education_qualification;
    private TextView take_image_image_of_interview_form,take_image_image_of_address_proof,take_image_image_of_id_proof,take_image_image_of_last_education_qualification;
    private EditText base_location;
    private EditText alternate_contact_number;

    public void commonInitialization()
    {
        context = AddCampFacilitator.this;
        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        name = (EditText) findViewById(R.id.name);

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        area_manager = (TextView) findViewById(R.id.area_manager);
        camp_coordinator = (TextView) findViewById(R.id.camp_coordinator);

        email_id = (EditText) findViewById(R.id.email_id);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        gender = (Spinner) findViewById(R.id.gender);
        date_of_birth = (TextView) findViewById(R.id.date_of_birth);
        pan_number = (EditText) findViewById(R.id.pan_number);
        education_qualification = (Spinner) findViewById(R.id.education_qualification);
        prior_work_experiance = (EditText) findViewById(R.id.prior_work_experiance);

        base_location = (EditText) findViewById(R.id.base_location);

        alternate_contact_number = (EditText) findViewById(R.id.alternate_contact_number);



        address = (EditText) findViewById(R.id.address);
        course = (TextView) findViewById(R.id.course);

        save = (TextView) findViewById(R.id.save);

    }

    TextView tempMultiselect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_camp_facilitator);
        commonInitialization();

//        new CommonFunction().setDependancy1(false, context, course, "courses", "name", null, null, null, null);
        new CommonFunction().setInformationInFormIfAvailable("camp_facilitator", context);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);


        adapter = ArrayAdapter.createFromResource(this, R.array.education_qualification, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        education_qualification.setAdapter(adapter);



        agendaInfo = new CommonFunction().getAgendaInfo(context);

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district", " where id='" + agendaInfo.optString("district") + "'", context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block", " where id='" + agendaInfo.optString("block") + "'", context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        area_manager.setText(new CommonFunction().getAreaManagerId(context));
        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));


        date_of_birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_birth.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_birth;

            }
        });




        course.setTag("[]");
        course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, MultiselectBox.class);
                in.putExtra("key_column", "id");
                in.putExtra("value_column", "name");

                JSONArray campLeaderList = new MDbHelper().getAll("courses", "", context);

                in.putExtra("multiselect_list", campLeaderList.toString());
                in.putExtra("selected_options", course.getTag().toString());
                startActivityForResult(in, 3);

                tempMultiselect = course;
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("name", name.getText().toString());
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("area_manager", area_manager.getText().toString());
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());

                        saveObject.put("email_id", email_id.getText().toString());
                        saveObject.put("base_location", base_location.getText().toString());
                        saveObject.put("mobile_number", mobile_number.getText().toString());
                        saveObject.put("gender", gender.getSelectedItem().toString());
                        saveObject.put("date_of_birth", date_of_birth.getText().toString());
                        saveObject.put("pan_number", pan_number.getText().toString());
                        saveObject.put("education_qualification", education_qualification.getSelectedItem().toString());
                        saveObject.put("prior_work_experiance", prior_work_experiance.getText().toString());
                        saveObject.put("alternate_contact_number", alternate_contact_number.getText().toString());

                        saveObject.put("course", course.getTag().toString());

                        // new CommonFunction().saveInformation(context, "camp_facilitator", saveObject);

                        new CommonFunction().saveInformation(context, "camp_facilitator", saveObject);

                        new CommonFunction().showAlertDialog("Data Saved Successfully", "", context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        new CommonFunction().imageClick(context, "t_image_of_camp_facilitator", "camp_facilitator");
        new CommonFunction().imageClick(context, "t_image_of_interview_form", "camp_facilitator");
        new CommonFunction().imageClick(context, "t_image_of_address_proof", "camp_facilitator");
        new CommonFunction().imageClick(context, "t_image_of_id_proof", "camp_facilitator");
        new CommonFunction().imageClick(context, "t_image_of_last_education_qualification", "camp_facilitator");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
        if(requestCode==3 && resultCode==1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("selected_options")==true) {
                tempMultiselect.setTag(data.getExtras().getString("selected_options"));

                try {
                    JSONArray selectedOptions = new JSONArray(data.getExtras().getString("selected_options"));
                    tempMultiselect.setText(""+selectedOptions.length()+" courses selected");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("name",new CommonFunction().rule_required);
            rules.put("mobile_number",new CommonFunction().rule_min_length+"=10");
            rules.put("email_id",new CommonFunction().rule_email);
            rules.put("base_location",new CommonFunction().rule_required);
            rules.put("alternate_contact_number",new CommonFunction().rule_min_length+"=10");

            rules.put("t_image_of_camp_facilitator",new CommonFunction().rule_image_required);
            rules.put("t_image_of_interview_form",new CommonFunction().rule_image_required);
            rules.put("t_image_of_address_proof",new CommonFunction().rule_image_required);
            rules.put("t_image_of_id_proof",new CommonFunction().rule_image_required);
            rules.put("t_image_of_last_education_qualification",new CommonFunction().rule_image_required);




//            rules.put("name",new CommonFunction().rule_required);
//            rules.put("name_of_person",new CommonFunction().rule_required);
//            rules.put("phone_number",new CommonFunction().rule_required);
//            rules.put("phone_number",new CommonFunction().rule_min_length+"=10");
//            rules.put("t_image_of_key_person",new CommonFunction().rule_image_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }
    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }



    @Override
    public void onBackPressed() {

        String validationString = "";

        CommonFunction commonFunction = new CommonFunction();
        commonFunction.subActivity = true;
        validationString = commonFunction.checkRequiredImages(rulesValidation(),"camp_facilitator",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }



}

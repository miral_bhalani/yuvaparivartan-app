package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class ArrangeIfrastructureCamp extends ActionBarActivity {


    public TextView tempDate;
    private TextView state, district, block;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;

    private TextView save;
    private TextView village;
    private EditText camp_code;
    public JSONObject agendaSingle = new JSONObject();

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private JSONObject agendaInfo;

    private EditText venue_details;
    private EditText rent_for_camp;




    public void commonInitialization()
    {
        context = ArrangeIfrastructureCamp.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);

        village = (TextView) findViewById(R.id.village);
        camp_code = (EditText) findViewById(R.id.camp_code);


        //**
        venue_details = (EditText) findViewById(R.id.venue_details);
        rent_for_camp = (EditText) findViewById(R.id.rent_for_camp);

        save = (TextView) findViewById(R.id.save);

    }



    TextView tempMultiselect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arrange_infrastructure_camp);
        commonInitialization();

        /* Display Date in correct format in agenda detail page - pass Shared Preferences and TextView and db field name for your concern
         * For agenda = agenda_date
         * Camp Start Date = camp_start_date
         * Tentative End Date = tentative_end_date
         * Actual End Date = actual_end_date;
         */


        agendaInfo = new CommonFunction().getAgendaInfo(context);

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block"," where id='"+agendaInfo.optString("block")+"'",context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
        village.setText(vilages.optJSONObject(0).optString("name"));


        String campId = "";
        //--get camp information from agenda camp id and set it on sharedpreferance
        try {
            campId = agendaInfo.getString("camp");

            if(campId.equals("")==false) {
                JSONArray campArray1 = new MDbHelper().getAll("camp", "", context);
                //            camp_facilitator_date.setText(campArray1.getJSONObject(0).getString("agenda_date"));
                //            System.out.println("!!!!pankaj_date_from_agenda_array"+campArray1.getJSONObject(0).getString("agenda_date"));
                JSONArray campArray = new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
                if (campArray.length() != 0) {
                    JSONObject campObject = campArray.getJSONObject(0);
                    sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"), campObject.toString());
                    sharededitor.commit();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



//        sharededitor.remove(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"));
//        sharededitor.commit();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    if (new CommonFunction().applyFormRules(rulesValidation(), fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("camp_code", camp_code.getText().toString());

                        saveObject.put("venue_details", venue_details.getText().toString().trim());
                        saveObject.put("rent_for_camp", rent_for_camp.getText().toString().trim());

                        new CommonFunction().saveInformation(context, "camp", saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        new CommonFunction().imageClick(context, "t_image_of_infrastructure", "camp");



        if(campId.equals("")==false) {
            new CommonFunction().setInformationInFormIfAvailable("camp", context);
        }
        else
        {
            sharededitor.remove(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"));
            sharededitor.commit();
        }

    }


    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
        }
        else if(controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }


    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("camp_code",new CommonFunction().rule_required);

            rules.put("venue_details",new CommonFunction().rule_required);
            rules.put("rent_for_camp",new CommonFunction().rule_required);
            rules.put("t_image_of_infrastructure",new CommonFunction().rule_image_required);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }



}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class DoorToDoorMeeting extends ActionBarActivity {


    private TextView state, district, block;
    public TextView meeting_date,tempDate,date_of_the_market;
    SpAdapter adapter;
    private Context context;
    private String last_selecteddate;
    private ArrayAdapter<String> spinnerAdapter;
    private JSONArray stateListArray;
    private JSONArray districtListArray;
    private TextView save;
    private TextView village;


    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private EditText camp_coordinator;
    private EditText number_of_households_visited;
    private EditText phone_number;
    private EditText number_of_target_groups_met;
    private JSONObject agendaInfo;
    private EditText outcome_of_meeting;

    EditText name_of_the_market,nearest_bank,any_other_land_mark_near_the_market;
    Spinner enrollment_done;
    private TextView take_image_mobilization;

    public void commonInitialization()
    {
        context = DoorToDoorMeeting.this;


        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();

        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);
        village = (TextView) findViewById(R.id.village);
        camp_coordinator = (EditText) findViewById(R.id.camp_coordinator);
        meeting_date = (TextView) findViewById(R.id.meeting_date);
        number_of_households_visited = (EditText) findViewById(R.id.number_of_households_visited);
        number_of_target_groups_met = (EditText) findViewById(R.id.number_of_target_groups_met);
        outcome_of_meeting = (EditText) findViewById(R.id.outcome_of_meeting);


        name_of_the_market = (EditText) findViewById(R.id.name_of_the_market);
        date_of_the_market = (TextView) findViewById(R.id.date_of_the_market);
        nearest_bank = (EditText) findViewById(R.id.nearest_bank);
        any_other_land_mark_near_the_market = (EditText) findViewById(R.id.any_other_land_mark_near_the_market);

        enrollment_done = (Spinner) findViewById(R.id.enrollment_done);


        save = (TextView) findViewById(R.id.save);

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.door_to_door_meeting);
        commonInitialization();



        agendaInfo = new CommonFunction().getAgendaInfo(context);

        meeting_date.setText(new CommonFunction().getDisplayDate(agendaInfo.optString("agenda_date")));

        JSONArray states = new MDbHelper().getAll("state"," where id='"+agendaInfo.optString("state")+"'",context);
        state.setText(states.optJSONObject(0).optString("state"));

        JSONArray districts = new MDbHelper().getAll("district"," where id='"+agendaInfo.optString("district")+"'",context);
        district.setText(districts.optJSONObject(0).optString("district"));

        JSONArray blocks = new MDbHelper().getAll("block"," where id='"+agendaInfo.optString("block")+"'",context);
        block.setText(blocks.optJSONObject(0).optString("block"));

        JSONArray vilages = new MDbHelper().getAll("village", " where id='" + agendaInfo.optString("village") + "'", context);
        village.setText(vilages.optJSONObject(0).optString("name"));

        camp_coordinator.setText(new CommonFunction().getCampCoordinatorId(context));





        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.enrollment_done, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        enrollment_done.setAdapter(adapter);


        date_of_the_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_the_market.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_the_market;

            }
        });



        //--SpinnerInitialization and dependancy specification
//        new CommonFunction().setDependancy1(false,context, state,"state","state",null,null,null,null);
//        new CommonFunction().setDependancy1(true, context, district, "district", "district", state, "state", "state", block);
//        new CommonFunction().setDependancy1(true, context, block, "block", "block", district, "district", "district", null);


//        new CommonFunction().setDependancy1(false, context, camp_coordinator, "camp_coordinator_profile", "name", null, null, null, null);


        meeting_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", meeting_date.getText().toString());
                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = meeting_date;

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try {

                    if(new CommonFunction().applyFormRules(rulesValidation(),fieldLabels(),true,context).equals("")==true) {
                        JSONObject saveObject = new JSONObject();
                        saveObject.put("state", agendaInfo.optString("state"));
                        saveObject.put("district", agendaInfo.optString("district"));
                        saveObject.put("block", agendaInfo.optString("block"));
                        saveObject.put("camp_coordinator", camp_coordinator.getText().toString());
                        saveObject.put("village", agendaInfo.optString("village"));
                        saveObject.put("meeting_date", meeting_date.getText().toString());
                        saveObject.put("number_of_households_visited", number_of_households_visited.getText().toString());
                        saveObject.put("number_of_target_groups_met", number_of_target_groups_met.getText().toString());
                        saveObject.put("outcome_of_meeting", outcome_of_meeting.getText().toString());

                        //---
//                        saveObject.put("name_of_the_market", name_of_the_market.getText().toString());
//                        saveObject.put("date_of_the_market", date_of_the_market.getText().toString());
//                        saveObject.put("nearest_bank", nearest_bank.getText().toString());
//                        saveObject.put("any_other_land_mark_near_the_market", any_other_land_mark_near_the_market.getText().toString());
//                        saveObject.put("enrollment_done", enrollment_done.getSelectedItem().toString());



                        new CommonFunction().saveInformation(context,"door_to_door_meeting",saveObject);


                        new CommonFunction().setAgendaDone(context);

                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });



//        new CommonFunction().imageClick(context, "t_image_of_canopy", "door_to_door_meeting");
//        new CommonFunction().imageClick(context, "t_image_of_door_to_door_meeting", "door_to_door_meeting");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }



    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("meeting_date",new CommonFunction().rule_required);
            rules.put("number_of_households_visited",new CommonFunction().rule_required);
            rules.put("number_of_target_groups_met",new CommonFunction().rule_required);
//            rules.put("t_image_of_canopy",new CommonFunction().rule_image_required);
//            rules.put("t_image_of_door_to_door_meeting",new CommonFunction().rule_image_required);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {


        } catch (Exception e) {
            e.printStackTrace();
        }
        return labels;
    }

    @Override
    public void onBackPressed() {

        String validationString = "";

        validationString = new CommonFunction().checkRequiredImages(rulesValidation(),"door_to_door_meeting",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }


}

package yuvaparivartan.app.yuvaparivartanandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import yuvaparivartan.app.yuvaparivartanandroid.dbhelpers.MDbHelper;


public class CampClosure extends ActionBarActivity {

    private EditText amount_of_fees_deposited;
    public TextView camp_start_date, tentative_end_date, actual_end_date, tempDate;
    private TextView state, district, block;

    private TextView save;

    SpAdapter adapter;
    List<String> listServ = new ArrayList<>();
    private Context context;
    private String last_selecteddate;
    private Spinner camp_status;
    private TextView village;
    private TextView camp_code;
    private JSONObject agendaInfo;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor sharededitor;
    private TextView current_fees_collection;
    private TextView total_of_fees_collected;
    private TextView name_of_person_holding_the_amount;
    private TextView name_of_person_holding_the_amount_phone_number;
    private TextView take_image;
    private String amount_of_fees_depositedString;
    private EditText reason;
    private EditText final_number_of_students;
    private EditText data_entry_completed_in_iris;
    private TextView date_of_submission;
    private Spinner voucher_submited;
    private LinearLayout reason_lin;

    public JSONObject dynamicRulesM = new JSONObject();
    private Spinner certificate_requisition_sent;
    private TextView certificate_email_sent_date;
    private LinearLayout certificate_email_sent_date_lin;

    @Override
    protected void onResume() {
        super.onResume();
            JSONArray vilages = new MDbHelper().getAll("village"," where id='"+agendaInfo.optString("village")+"'",context);
            village.setText(vilages.optJSONObject(0).optString("name"));
    }

    public void commonInitialization() {
        context = CampClosure.this;
        sharedpreferences = getSharedPreferences("MyPref", 0);
        sharededitor = sharedpreferences.edit();


        reason_lin = (LinearLayout) findViewById(R.id.reason_lin);



        state = (TextView) findViewById(R.id.state);
        district = (TextView) findViewById(R.id.district);
        block = (TextView) findViewById(R.id.block);


        village = (TextView) findViewById(R.id.village);
        camp_code = (TextView) findViewById(R.id.camp_code);

        camp_start_date = (TextView) findViewById(R.id.camp_start_date);
        tentative_end_date = (TextView) findViewById(R.id.tentative_end_date);

        actual_end_date = (TextView) findViewById(R.id.actual_end_date);
        certificate_email_sent_date = (TextView) findViewById(R.id.certificate_email_sent_date);
        

        camp_status = (Spinner) findViewById(R.id.camp_status);
        certificate_requisition_sent = (Spinner) findViewById(R.id.certificate_requisition_sent);

        reason = (EditText) findViewById(R.id.reason);


        final_number_of_students = (EditText) findViewById(R.id.final_number_of_students);

        certificate_email_sent_date_lin = (LinearLayout) findViewById(R.id.certificate_email_sent_date_lin);

        data_entry_completed_in_iris = (EditText) findViewById(R.id.data_entry_completed_in_iris);



        amount_of_fees_deposited = (EditText) findViewById(R.id.amount_of_fees_deposited);



        date_of_submission = (TextView) findViewById(R.id.date_of_submission);

        voucher_submited = (Spinner) findViewById(R.id.voucher_submited);
        
        save = (TextView) findViewById(R.id.save);


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camp_closure);
        commonInitialization();

        try {
            agendaInfo = new JSONObject(sharedpreferences.getString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("agenda"), ""));
            String campId = agendaInfo.getString("camp");
            JSONArray campArray1 =  new MDbHelper().getAll("camp", "", context);
            JSONArray campArray =  new MDbHelper().getAll("camp", " where id='" + campId + "'", context);
            if(campArray.length()!=0)
            {
                JSONObject campObject = campArray.getJSONObject(0);
                sharededitor.putString(new CommonFunction().getSheredPreferanceDetailsKeyForTable("camp"),campObject.toString());
                sharededitor.commit();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        new CommonFunction().setInformationInFormIfAvailable("camp", context);




        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.camp_status, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        camp_status.setAdapter(adapter);



        adapter = ArrayAdapter.createFromResource(this, R.array.voucher_submited, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        voucher_submited.setAdapter(adapter);



        adapter = ArrayAdapter.createFromResource(this, R.array.certificate_requisition_sent, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        certificate_requisition_sent.setAdapter(adapter);




        date_of_submission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", date_of_submission.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = date_of_submission;

            }
        });

        actual_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", actual_end_date.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = actual_end_date;

            }
        });


        certificate_email_sent_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(context, DatePickerM.class);
                in.putExtra("date", certificate_email_sent_date.getText().toString());
//                in.putExtra("min_date", String.valueOf(System.currentTimeMillis()));
//                in.putExtra("update_Date", last_selecteddate);
                startActivityForResult(in, 1);
                tempDate = certificate_email_sent_date;

            }
        });



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                dynamicRulesM = new CommonFunction().mergeJsonObjects(rulesValidation(),dynamicRulesM);
                try {
                    if (new CommonFunction().applyFormRules(dynamicRulesM, fieldLabels(), true, context).equals("") == true) {
                        JSONObject saveObject = new JSONObject();

                        saveObject.put("actual_end_date", actual_end_date.getText().toString());
                        saveObject.put("reason", reason.getText().toString());
                        saveObject.put("final_number_of_students", final_number_of_students.getText().toString());
                        saveObject.put("data_entry_completed_in_iris", data_entry_completed_in_iris.getText().toString());
                        saveObject.put("date_of_submission", date_of_submission.getText().toString());
                        saveObject.put("amount_of_fees_deposited", amount_of_fees_deposited.getText().toString());
                        saveObject.put("camp_status", camp_status.getSelectedItem().toString());
                        saveObject.put("voucher_submited", voucher_submited.getSelectedItem().toString());
                        saveObject.put("date_of_submission", date_of_submission.getText().toString());
                        saveObject.put("certificate_requisition_sent", certificate_requisition_sent.getSelectedItem().toString());
                        saveObject.put("certificate_email_sent_date", certificate_email_sent_date.getText().toString());
                        saveObject.put("voucher_submited", voucher_submited.getSelectedItem().toString());

                        new CommonFunction().saveInformation(context, "camp", saveObject);


                        new CommonFunction().setAgendaDone(context);


                        new CommonFunction().showAlertDialog("Data Saved successfully", "", context);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        new CommonFunction().changeVisibilityOfSomeLin(camp_status, reason_lin, "Aborted", null, "camp_status_positive", "camp_status_negative", context);
        camp_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new CommonFunction().changeVisibilityOfSomeLin(camp_status, reason_lin, "Aborted", null, "camp_status_positive", "camp_status_negative", context);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        new CommonFunction().changeVisibilityOfSomeLin(certificate_requisition_sent, certificate_email_sent_date_lin, "Yes", "No", "certificate_email_sent_positive", "certificate_email_sent_negative",context);
        certificate_requisition_sent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new CommonFunction().changeVisibilityOfSomeLin(certificate_requisition_sent, certificate_email_sent_date_lin, "Yes", "No", "certificate_email_sent_positive", "certificate_email_sent_negative",context);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void camp_status_positive()
    {
        try {
            dynamicRulesM.put("reason",new CommonFunction().rule_required);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void camp_status_negative()
    {
        try {
            dynamicRulesM.remove("reason");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void certificate_email_sent_positive()
    {
        try {
            dynamicRulesM.put("certificate_email_sent_date",new CommonFunction().rule_required);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void certificate_email_sent_negative()
    {
        try {
            dynamicRulesM.remove("certificate_email_sent_date");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    public void changeVisibilityOfSomeLin(final Spinner controllerView, final View visibilityView,  final String visibleValue, final String hideValue,String positiveFunc,String negativeFunc)
    {
        if(controllerView.getSelectedItem().toString().equals(visibleValue))
        {
            visibilityView.setVisibility(View.VISIBLE);
            Method m = null;
            try {
                if(positiveFunc!=null) {
                    m = Class.forName(CampClosure.class.getName()).getDeclaredMethod(positiveFunc);
                    m.invoke(CampClosure.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(hideValue!=null && controllerView.getSelectedItem().toString().equals(hideValue))
        {
            visibilityView.setVisibility(View.GONE);
            Method m = null;
            try {
                if(negativeFunc!=null) {
                    m = Class.forName(CampClosure.class.getName()).getDeclaredMethod(negativeFunc);
                    m.invoke(new CampClosure());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(hideValue==null)
        {
            visibilityView.setVisibility(View.GONE);
            Method m = null;
            try {
                if(negativeFunc!=null) {
                    m = Class.forName(CampClosure.class.getName()).getDeclaredMethod(negativeFunc);
                    m.invoke(new CampClosure());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public JSONObject rulesValidation()
    {
        JSONObject rules = new JSONObject();
        try {

            rules.put("amount_of_fees_deposited",new CommonFunction().rule_required);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  rules;
    }

    public JSONObject fieldLabels()
    {
        JSONObject labels = new JSONObject();
        try {

            labels.put("amount_of_fees_deposited","Ammount Of Fees Deposited");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return labels;
    }





    @Override
    public void onBackPressed() {

        String validationString = "";

        validationString = new CommonFunction().checkRequiredImages(rulesValidation(),"camp",context);
        if(validationString.equals("")==false)
        {
            new CommonFunction().showAlertDialog(validationString,"",context);
        }
        else
        {
            super.onBackPressed();
        }

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == 1)
        {
            if(data != null && data.getExtras() != null && data.getExtras().containsKey("date")==true) {
                tempDate.setText(data.getExtras().getString("date"));
            }
        }
    }


}
